/**
 * Mock panel as a display and placeholder for other panels.
 */
class Mock extends Panel
{
  /**
   * Handle the mouse down event.
   */
  onMouseDown(mouse)
  {
    console.log("Mouse down");
    console.log(mouse);
  }

  /**
   * Handle the mouse down event.
   */
  onMouseUp(mouse)
  {
    console.log("Mouse up");
    console.log(mouse);
  }

  /**
   * Handle the key down event.
   */
  onKeyDown(key)
  {
    console.log("Key down");
    console.log(key);
  }

  /**
   * Handle the key up event.
   */
  onKeyUp(key)
  {
    console.log("Key up");
    console.log(key);
  }

  /**
   * Draw the diagonals.
   */
  draw(context)
  {
    context.fillStyle = "#EEF";
    context.fillRect(0, 0, this.width, this.height);
    context.beginPath();
    context.moveTo(0, 0);
    context.lineTo(this.width, this.height);
    context.stroke()
    context.beginPath();
    context.moveTo(this.width, 0);
    context.lineTo(0, this.height);
    context.stroke();
  }
}

