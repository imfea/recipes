/**
* Initialize the content of the canvas.
*/
function initContent()
{
  var mock = new Mock();
  var testBench = new TestBench(mock);
  screen.setRoot(testBench);
}

