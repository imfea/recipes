/**
 * Represents a dragging operation.
 */
class Dragging
{
  /**
   * Construct a new dragging operation.
   */
  constructor(mode, obj, panel, mouse)
  {
    this.mode = mode;
    this.obj = obj;
    this.panel = panel;
    // Mouse event at the start of the dragging.
    this.mouse = mouse;
    // Offset for mouse position calculations.
    this.offset = null;
  }
}

