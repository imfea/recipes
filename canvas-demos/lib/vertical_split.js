/**
 * Vertically splitted panel.
 */
class VerticalSplit extends Panel
{
    /**
     * Construct a new vertical splitter.
     */
    constructor()
    {
        super();
        this._splitX = 100;
        this._isSplitterFocused = false;
    }

    resize()
    {
        if (this.children.length == 2) {
            this.children[0].x = 0;
            this.children[0].y = 0;
            this.children[0].width = this._splitX;
            this.children[0].height = this.height;
            this.children[1].x = this._splitX;
            this.children[1].y = 0;
            this.children[1].width = this.width - this._splitX;
            this.children[1].height = this.height;
            this.children[0].isRedrawNeeded = true;
            this.children[1].isRedrawNeeded = true;
            this.requireRedraw();
        }
    }

    /**
     * Find the panel and its local coordinates at the position
     * of the mouse pointer.
     */
    findCursor(position)
    {
        if (Math.abs(position.x - this._splitX) < 10) {
            if (this._isSplitterFocused == false) {
                this._isSplitterFocused = true;
                this.children[0].isRedrawNeeded = true;
                this.children[1].isRedrawNeeded = true;
                this.requireRedraw();
            }
            let cursor = {
                x: position.x,
                y: position.y,
                panel: this
            };
            return cursor;
        } else {
            if (this._isSplitterFocused == true) {
                this._isSplitterFocused = false;
                this.children[0].isRedrawNeeded = true;
                this.children[1].isRedrawNeeded = true;
                this.requireRedraw();
            }
            return super.findCursor(position);
        }
    }

    /**
    * Handle the mouse down event.
    */
    onMouseDown(mouse)
    {
        if (Math.abs(mouse.x - this._splitX) < 10) {
            let dragging = new Dragging("internal", null, this, mouse);
            this.startDragging(dragging);
        }
    }

    /**
     * Called on mouse move event during a dragging operation.
     */
    onDragging(dragging, mouse)
    {
        this._splitX = mouse.x;
        if (this._splitX < 0) {
            this._splitX = 0;
        }
        if (this._splitX > this.width) {
            this._splitX = this.width;
        }
        this.resize();
    }

    /**
    * Handle the mouse move event.
    */
    onMouseMove(mouse)
    {
        // console.log('Move on the splitter!');
        /*
        if (Math.abs(mouse.x - this._splitX) < 10) {
            console.log('Focused');
        } else {
            console.log('Out of focus!');
        }
        */
    }

    /**
     * Draw the content of the panel.
     */
    drawOverlay(context)
    {
        // console.log('Draw me!');
        if (this._isSplitterFocused) {
            context.fillStyle = "#448";
            context.fillRect(this._splitX - 2, 0, 5, this.height);
        }
    }
}

