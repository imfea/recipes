# Drag & Drop

The Drag & Drop functionality is an essential part of the user interface.

There are two types (*mode*) of the them:

* *internal*: The drop target is the same panel, regardless of the position of the mouse.
* *external*: The drop target is the panel which is under the mouse cursor, when the mouse button has released.

We can distinguish the source and the target panel of the operation. When the source panel receives a mouse down event, it can explicitly call the start of the dragging operation by the method `Panel.startDragging`. It requires a `Dragging` object as its argument.

We can instantiate a `Dragging` as
```javascript
let dragging = new Dragging(mode, obj, panel, mouse);
```
where the meaning of the parameters is the following:

* `mode`: a string value which can be `"internal"` or `"external"`,
* `obj`: the dragged object (or `null` when not relevant),
* `panel`: reference to the source panel,
* `mouse`: mouse coordinates at the start of the dragging.

The reference of the source panel is necessary for calling back the source via the method `onDrop`, when the drag & drop operation has finished.

## Events while dragging

The source panel manages the whole drag & drop operation. Simultaneously to the `onMouseMove` event, it calls the method `onDragging`. It makes available continuous refresh, which could be necessary for example for the resizing operations.

The method `onDragging` receives the following arguments:

* `dragging`: the `Dragging` instance,
* `mouse`: the actual position of the mouse.

The mouse position is depends on the type of the dragging.



## Events when drop

When the dragging has finished, the method `Panel.onDrop` has been called. It is working practically the same way as the callback `onDragging`. The only difference is that, this call signs the end of the drag & drop operation.

When there is no relevance of the last step (for example on a simple moving operation), this call can be ignored.

