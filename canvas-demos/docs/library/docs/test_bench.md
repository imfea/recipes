# Usage of the test bench

The library encourages component-based development. When possible, You should develope the panels (as the components of your application) separately. The test bench is also a `Panel`, which makes easier to try out your currently developed panel. It provides

* resize event,
* mouse events,
* key events.

You can use the test bench in the following steps.

* Derive your class from the class `Panel`!
* Place the file which contains the class definition into the sources of the test bench project!
* In the file `main.js` modify the line
```javascript
var mock = new Mock();
```
for instantiating your own panel class.

!!! note

    It is also possible to alter the definition of the class `Mock`.

