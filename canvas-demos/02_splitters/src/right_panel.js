class RightPanel extends Panel
{
  /**
   * Draw a green coloured background.
   */
  draw(context)
  {
    context.fillStyle = "#EFE";
    context.fillRect(0, 0, this.width, this.height);
  }

  onMouseDown(event)
  {
      console.log('Right: (' + event.x + ', ' + event.y + ')');
  }

  /**
  * Handle the mouse move event.
  */
  onMouseMove(mouse)
  {
    console.log('Move on right!');
  }
}

