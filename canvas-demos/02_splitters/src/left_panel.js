class LeftPanel extends Panel
{
  /**
   * Draw a red coloured background.
   */
  draw(context)
  {
    context.fillStyle = "#FEE";
    context.fillRect(0, 0, this.width, this.height);
  }

  onMouseDown(event)
  {
      console.log('Left: (' + event.x + ', ' + event.y + ')');
  }

  /**
  * Handle the mouse move event.
  */
  onMouseMove(mouse)
  {
    console.log('Move on left!');
  }
}

