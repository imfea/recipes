/**
 * Test bench for efficient panel development.
 */
class TestBench extends Panel
{
  /**
   * Construct a new test bench.
   */
  constructor()
  {
    super();
    this._dimension = null;
    this._padding = 40;
    this._mock = new VerticalSplit();
    this._mock.addChild(new LeftPanel());
    this._mock.addChild(new RightPanel());
    this._mock.x = this._padding;
    this._mock.y = this._padding;
    this._mock.width = 300;
    this._mock.height = 200;
    this.addChild(this._mock);
  }

  /**
   * Handle the mouse down event.
   */
  onMouseDown(mouse)
  {
    if (mouse.button == 0) {
      let w = mouse.x - (2 * this._padding);
      let h = mouse.y - (2 * this._padding);
      if (w > 0 && h > 0) {
        this._mock.width = w;
        this._mock.height = h;
        // NOTE: It requires the `_resize` as a recursive call for complex panels.
        this._mock._resize();
      }
    }
  }

  /**
   * Handle the key down event.
   */
  onKeyDown(key)
  {
    if (key.key == ' ') {
        this._mock.width = 64;
        this._mock.height = 64;
        // NOTE: It requires the `_resize` as a recursive call for complex panels.
        this._mock._resize();
    }
    console.log(key);
  }

  /**
   * Draw the test bench.
   */
  draw(context)
  {
    context.fillStyle = "#ABC";
    context.fillRect(0, 0, this.width, this.height);
    context.fillStyle = "#FFF";
    context.fillRect(
      0,
      0,
      this._mock.width + 2 * this._padding,
      this._mock.height + 2 * this._padding
    );
  }
}

