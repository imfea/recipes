# Dragging

There are two types of the dragging:
* *internal*: The drop target is the same panel, regardless of the position of the mouse.
* *external*: The drop target is the panel which is under the mouse cursor, when the mouse button has released.

## Start dragging

mode
dragged object
// dragSource

## onDrop

Called on the drop target.

x, y
dragged object
dragSource

