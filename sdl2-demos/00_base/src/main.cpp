#include "app.h"

#include <iostream>
#include <unistd.h>

const int WIDTH = 800;
const int HEIGHT = 600;
const int MAIN_LOOP_SLEEP = 1000;

/**
 * Main function
 */
int main(int argc, char* argv[])
{
  App app(WIDTH, HEIGHT);

  while (app.isRunning()) {
    app.handleEvents();
    app.update();
    app.render();
    usleep(MAIN_LOOP_SLEEP);
  }

  return 0;
}

