#include "app.h"

#include <iostream>
#include <stdexcept>

App::App(int width, int height)
{
  int errorCode = SDL_Init(SDL_INIT_EVERYTHING);
  if (errorCode != 0) {
    throw std::runtime_error(SDL_GetError());
  }

  _window = SDL_CreateWindow(
    "App",
    SDL_WINDOWPOS_CENTERED,
    SDL_WINDOWPOS_CENTERED,
    width,
    height,
    0
  );

  _width = width;
  _height = height;

  _renderer = SDL_CreateRenderer(_window, -1, SDL_RENDERER_ACCELERATED);

  _needToRun = true;
}

App::~App()
{
  SDL_DestroyRenderer(_renderer);
  SDL_DestroyWindow(_window);
  SDL_Quit();
}

void App::handleEvents()
{
  std::cout << "Handle events" << std::endl;

  SDL_Event event;

  while (SDL_PollEvent(&event)) {
    switch (event.type) {
    case SDL_KEYDOWN:
      switch (event.key.keysym.scancode) {
      case SDL_SCANCODE_ESCAPE:
        _needToRun = false;
        break;
      case SDL_SCANCODE_CAPSLOCK:
        _needToRun = false;
        break;
      }
      break;
    case SDL_QUIT:
      _needToRun = false;
      break;
    }
  }
}

void App::update()
{
  std::cout << "Update" << std::endl;
}

void App::render()
{
  std::cout << "Render" << std::endl;

  SDL_SetRenderDrawColor(_renderer, 16, 16, 16, 255);
  SDL_Rect rect = {0, 0, _width, _height};
  SDL_RenderFillRect(_renderer, &rect);
  SDL_RenderPresent(_renderer);
}

bool App::isRunning() const
{
  return _needToRun;
}

