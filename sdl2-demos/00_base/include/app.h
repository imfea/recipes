#ifndef APP_H
#define APP_H

#include <SDL2/SDL.h>

class App
{
public:

  /**
   * Construct a new application.
   */
  App(int width, int height);

  /**
   * Destruct the application.
   */
  virtual ~App();

  /**
   * Handle the events of the application.
   */
  void handleEvents();

  /**
   * Update the application.
   */
  void update();

  /**
   * Render the application.
   */
  void render();

  /**
   * Is the application still running?
   */
  bool isRunning() const;

private:

  SDL_Window* _window;
  SDL_Renderer* _renderer;

  int _width;
  int _height;

  bool _needToRun;
};

#endif /* APP_H */

