.. recipes documentation master file, created by
   sphinx-quickstart on Wed Sep 18 10:22:23 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Recipes
=======

Notes, examples, templates, code snippets, know-hows and tutorials

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   pages/html_canvas
   pages/file_uploading
   pages/html_resize
   pages/annotator/annotator
   pages/vue
   pages/wifi
   pages/ssh
   pages/bash
   pages/vim


