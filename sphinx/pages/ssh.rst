SSH configuration
=================

Create SSH keys
---------------

Generate the SSH key pair with the following command:

.. code:: bash

    ssh-keygen -t ed25519 -f <PRIVATE_KEY_PATH>

.. note::

    When use a passhprase, you should set the rounds by the ``-a`` option.

Temporary session
-----------------

Add SSH key to the current session:

.. code:: bash

    eval $(ssh-agent)
    ssh-add <PRIVATE_KEY_PATH>

Passwordless access
-------------------

You can use the ``ssh-copy-id`` command. It will append the public key to the end of the following file:

.. code:: bash

   ~/.ssh/authorized_keys

.. note::

   It also can be edited manually.

Set SSH key for git
-------------------

Add the following item into the ``.ssh/config`` file:

.. code:: bash

    Host <ARBITRARY_HOST_NAME>
        HostName <REAL_HOST_NAME>
        User git
        Port 22
        IdentityFile <PRIVATE_KEY_PATH>

Check the access of the files!

.. code:: bash

    stat -c %a <PATH>

* ``~/.ssh/config`` should be 600.
* ``<PRIVATE_KEY_PATH>`` should be 400.

.. warning::

   In the ``.git/config`` of the repository the path must contains the ``<ARBITRARY_HOST_NAME>`` (instead of ``<REAL_HOST_NAME>``)!

