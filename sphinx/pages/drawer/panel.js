/**
 * Represents a rectangular shaped panel.
 */
class Panel
{
    /**
     * Construct a new panel.
     */
    constructor()
    {
        this.x = 0;
        this.y = 0;
        this.width = 0;
        this.height = 0;
        this.children = [];
        this.isRedrawNeeded = true;
    }

    /**
     * Find panel at the given position.
     */
    findChildAt(position)
    {
        for (let child of this.children) {
            if (position.x >= child.x &&
                position.x < child.x + child.width &&
                position.y >= child.y &&
                position.y < child.y + child.height
            ) {
                return child;
            }
        }
        return null;
    }

    /**
     * Update the content according to the actual size of the panel.
     */
    onResize()
    {
    }

    /**
     * Delegate the mouse down event to the children.
     */
    delegateMouseDown(event)
    {
        let child = this.findChildAt(event);
        if (child != null) {
            let delegatedEvent = {
                x: event.x - child.x,
                y: event.y - child.y,
                button: event.button
            };
            child.onMouseDown(delegatedEvent);
            return true;
        }
        else {
            return false;
        }
    }

    /**
     * Handle the mouse down event.
     */
    onMouseDown(event)
    {
        let isDelegated = this.delegateMouseDown(event);
        if (isDelegated == false) {
            console.log('Mouse down at (' + event.x + ', ' + event.y + ')');
        }
    }

    onMouseMove(event)
    {
    }

    onMouseUp(event)
    {
    }

    onKeyDown(event)
    {
    }

    onKeyUp(event)
    {
    }

    /**
     * Draw the children of the panel.
     */
    drawChildren(context)
    {
        for (let child of this.children) {
            if (child.isRedrawNeeded) {
                context.save();
                context.translate(child.x, child.y);
                child.draw(context);
                context.restore();
            }
        }
    }

    /**
     * Draw the content of the panel.
     */
    draw(context)
    {
        this.drawChildren(context);
    }
}
