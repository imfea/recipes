import os

import falcon
import waitress


app = falcon.API()
app.add_static_route('/', os.getcwd())

waitress.serve(app, host='0.0.0.0', port=5000)

