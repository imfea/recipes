import json

import falcon
import waitress


class LoginResource:
    def on_post(self, req, resp):
        data = json.loads(req.stream.read().decode('utf-8'))
        print(data)
        message = {
          "token": "ok"
        }
        resp.body = json.dumps(message)
        resp.content_type = 'application/json'
        resp.status = falcon.HTTP_200


login_resource = LoginResource()

app = falcon.API()

app.add_route('/login', login_resource)

waitress.serve(app, host='127.0.0.1', port=5000)

