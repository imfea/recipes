Fit HTML Canvas Size to Window
==============================

This code is suitable as the skeleton of desktop applications.

.. literalinclude:: fit_canvas.html
   :language: html
