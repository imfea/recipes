WiFi connection
===============

This page describes how to manage WiFi access points and network connections from command line via ``wpa_cli``.

List the access points
----------------------

.. code:: bash

    > scan

It should be write ``OK`` after a successful scanning.
You can check the results as

.. code:: bash

    > scan_results


Add a new network
-----------------

At first, create a new uninitialized network:

.. code:: bash

    > add_network

It will print the provided network id. Let denote it by ``ID``.

For setting the SSID and the password:

.. code:: bash

    > set_network ID ssid "Write the SSID here!"
    > set_network ID psk "Write the password here!"


Enable the network
------------------

You should enable the network when it is not enabled by default.
List the networks for checking its status.

.. code:: bash

    > list_networks

For enabling the network:

.. code:: bash

    > enable_network ID

For selecting the required network:

.. code:: bash

    > select_network ID

