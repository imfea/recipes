/**
 * Create a new annotator instance.
 */
function Annotator()
{
  this._image = null;
  this._shiftX = 0;
  this._shiftY = 0;
  this._scale = 1;
  this._scalingFactor = 1.1;
  this._mousePosition = {"x": 0, "y": 0};
  this._mouseButtons = [false, false, false];
}

/**
 * Set the size of the annotator.
 */
Annotator.prototype.setSize = function(width, height)
{
  this._width = width;
  this._height = height;
  this.draw();
}

/**
 * Set the drawing context of the annotator.
 */
Annotator.prototype.setContext = function(context)
{
  this._context = context;
}

/**
 * Set the displayed image.
 */
Annotator.prototype.setImage = function(image)
{
  this._image = image;
  this._shiftX = 0;
  this._shiftY = 0;
  this._scale = 1;
  this.draw();
}

/**
 * Handle the mouse down event.
 */
Annotator.prototype.mouseDown = function(mouse)
{
  if (mouse["button"] == 0) {
    var realPosition = this.calcImageCoordinates(mouse);
    // NOTE: The realPosition is the position in the coordinate system of the image.
  }
  this._mousePosition = mouse;
  this._mouseButtons[mouse["button"]] = true;
}

/**
 * Handle the mouse move event.
 */
Annotator.prototype.mouseMove = function(mouse)
{
  const deltaX = mouse["x"] - this._mousePosition["x"];
  const deltaY = mouse["y"] - this._mousePosition["y"];
  if (this._mouseButtons[1]) {
    this.drag(deltaX, deltaY);
  }
  this._mousePosition = mouse;
}

/**
 * Handle the mouse up event.
 */
Annotator.prototype.mouseUp = function(mouse)
{
  this._mouseButtons[mouse["button"]] = false;
}

/**
 * Handle the mouse move event.
 */
Annotator.prototype.mouseWheel = function(mouse, delta)
{
  if (delta < 0) {
    this.zoomIn(mouse["x"], mouse["y"]);
  }
  else {
    this.zoomOut(mouse["x"], mouse["y"]);
  }
}

/**
 * Drag the view.
 */
Annotator.prototype.drag = function(x, y)
{
  this._shiftX += x;
  this._shiftY += y;
  this.draw();
}

/**
 * Zoom in to the given position.
 */
Annotator.prototype.zoomIn = function(x, y)
{
  this._shiftX = x - (x - this._shiftX) * this._scalingFactor;
  this._shiftY = y - (y - this._shiftY) * this._scalingFactor;
  this._scale *= this._scalingFactor;
  this.draw();
}

/**
 * Zoom out from the given position.
 */
Annotator.prototype.zoomOut = function(x, y)
{
  this._shiftX = x - (x - this._shiftX) / this._scalingFactor;
  this._shiftY = y - (y - this._shiftY) / this._scalingFactor;
  this._scale /= this._scalingFactor;
  this.draw();
}

/**
 * Calculate the real coordinates on the image.
 */
Annotator.prototype.calcImageCoordinates = function(mouse)
{
  var x = (mouse["x"] - this._shiftX) / this._scale;
  var y = (mouse["y"] - this._shiftY) / this._scale;
  return {
    "x": x,
    "y": y
  };
}

/**
 * Draw the annotator.
 */
Annotator.prototype.draw = function()
{
  if (this._context == null) {
    return;
  }

  this._context.resetTransform();
  this._context.clearRect(0, 0, this._width, this._height);
  this._context.translate(this._shiftX, this._shiftY);
  this._context.scale(this._scale, this._scale);

  this.drawImage();
}

/**
 * Draw the image.
 */
Annotator.prototype.drawImage = function()
{
  if (this._image == null) {
    return;
  }
  this._context.drawImage(this._image, 0, 0);
}
