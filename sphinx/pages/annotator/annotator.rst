Image Annotator
===============

.. warning::

   Change the image path in the HTML source!


HTML source
-----------

.. literalinclude:: annotator.html
   :language: html


JavaScript source
-----------------

.. literalinclude:: annotator.js
   :language: javascript
