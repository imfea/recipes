Vim configuration
=================

Install plugins
---------------

.. code::

	mkdir -p ~/.vim/pack/plugins/start
	cd ~/.vim/pack/plugins/start
	git clone https://github.com/editorconfig/editorconfig-vim.git
	git clone https://github.com/preservim/nerdtree.git
	git clone https://github.com/easymotion/vim-easymotion


Install color schemes
---------------------

Copy the color scheme file (with `.vim` extension) to the `~/.vim/colors` directory!

