/**
 * Service class for a simplified JSON-RPC communication via HTTP.
 */
class Service
{
    /**
     * Construct a new service instance.
     */
    constructor(urlPrefix, urlPostfix = '')
    {
        this._urlPrefix = urlPrefix;
        this._urlPostfix = urlPostfix;
    }

    /**
     * Call the remote service.
     */
    call(request)
    {
        const url = this._urlPrefix + request.method + this._urlPostfix;
        fetch(url, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(request.params)
        })
        .then(response => response.json())
        .then(request.onSuccess)
        .catch(request.onError);
    }
}
