Service
=======

Simplified RPC-style communication.

Instantiation
-------------

.. code:: javascript

    let $service = new Service("https://imfea.com/api/");

Usage
-----

.. code:: javascript

    $service.call({
        method: 'users/collect',
        params: {
            x: 8
        },
        onSuccess(response) {
            console.log('Success:', response);
        },
        onError(error) {
            console.error('Error:', error);
        }
    });

Dialects
--------

For different backend programming languages and frameworks we should use specific conventions.

The followings are rather advices, than suggested conventions.

.. TODO: Create examples for the following backend approaches!

PHP - domain/method approach
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

For example, in the directory :code:`users` we can use the PHP script :code:`collect`.

PHP - method as a parameter approach
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

We can consider the parameter name :code:`method` as a reserved name, and use as the first parameter. In this case we can use the script :code:`users.php` and implement the method related routing manually.

Python
~~~~~~

We can rely on a web framework. Most of them prefer REST API, but it is easy to adapt the RPC-style approach.

Go
~~

Rust
~~~~

Zig
~~~

