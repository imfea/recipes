BASH configuration
==================

.. code::

   function mkcd
   {
       mkdir $1 && cd $1
   }

   alias ..="cd .."
   alias ...="cd ../.."
   alias ....="cd ../../.."
   alias ..3="cd ../../.."
   alias ..4="cd ../../../.."
   alias ..5="cd ../../../../.."

   alias l="ls"
   alias ll="ls -l"
   alias la="ls -a"
   alias lla="ls -la"

   alias d="echo -e '\0033\0143'"

   alias activate="cd; source .venv/bin/activate; cd -"

