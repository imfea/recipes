File Uploading with Falcon
==========================

Requirements
------------

The ``requirements.txt`` should contain the following lines.

.. code-block::

   falcon
   falcon-multipart
   waitress


Minimal server
--------------

.. code-block:: python

    import falcon
    from falcon_multipart.middleware import MultipartMiddleware
    import waitress


    class Uploader(object):

        def on_get(self, req, resp):
            resp.body = """
            <html>
            <b>File uploading</b>
            <form action="/uploader" method="POST" enctype="multipart/form-data">
              <input type="file" name="image" id="image-id">
              <input type="submit" value="Upload">
            </form>
            </html>"""
            resp.content_type = falcon.MEDIA_HTML
            resp.status = falcon.HTTP_200

        def on_post(self, req, resp):
            image = req.get_param('image')
            filename = image.filename
            print(f'filename = {filename}')
            with open('/tmp/result.png', 'wb') as result_file:
                result_file.write(image.file.read())
            resp.body = 'The file has been successfully uploaded!'
            resp.status = falcon.HTTP_200


    api = application = falcon.API(middleware=[MultipartMiddleware()])
    api.req_options.auto_parse_form_urlencoded = True

    uploader = Uploader()
    api.add_route('/uploader', uploader)

    waitress.serve(api, host='0.0.0.0', port=8080)


Testing in the browser
----------------------

You will find the server at http://127.0.0.1:8080/uploader.

Curl script for uploading
-------------------------

The following command assumes that there is a ``sample.png`` in the current directory.

.. code-block:: bash

   curl -X POST -F "image=@sample.png;type=image/png" http://127.0.0.1:8000/uploader
