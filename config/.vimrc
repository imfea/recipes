" System clipboard
set clipboard=unnamedplus

" Syntax
syntax on
filetype indent on
filetype plugin on

" Indentation
set shiftwidth=4
set tabstop=4
set expandtab
set incsearch
set hlsearch
set nu
set hidden

" GVim disables
set guioptions-=m " menu
set guioptions-=L " left scroll
set guioptions-=T " toolbar
set guioptions-=r " right scroll

" Colorscheme
colorscheme darkburn

" NERDTree
map é :w<CR>
map ü :wqa<CR>
map ö :NERDTree<CR>
map í <C-w>

" Mouse
set mouse=a

" EasyMotion
let g:EasyMotion_leader_key='-'

" NERDTree
let NERDTreeIgnore = ['\.pyc$']
let g:NERDTreeMouseMode = 3

" Close the current buffer
map <C-c> :BW<cr>

" Mail and MarkDown editing
vmap q :s/^/> /g<CR>:let @/=""<CR>
vmap . <Esc>:%y<CR>
vmap , <Esc>:%d<CR>
vmap - <Esc>yypv^$:s/./-/g<CR>:let @/=""<CR>o<CR>
