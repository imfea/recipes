#!/bin/bash
#
# Calculate MD5 checksums per files.
#
# It requires the path of a directory as argument!
#
# * Redirect its output to a file, for example:
#
#   $ sh check_summer.sh my_archives > orig_checksums.txt
#
# * Compare the resulted checksums after archiving:
#
#   $ diff orig_checksums.txt resulted_checksums.txt
#

if [ $# -ne 1 ]; then
    echo 'ERROR: The script requires a path for checking as argument!'
    exit 1
fi

find $1 -type f | sort | while read fpath; do
    md5sum "$fpath"
done

