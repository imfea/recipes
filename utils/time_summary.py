import sys

def count_minutes(line):
    """
    Count the minutes in the given interval.
    """
    line = line.strip()
    assert line.count('-') == 1
    start_time, finish_time = line.split('-')
    start_minute = 0
    finish_minute = 0
    if ':' in start_time:
        start_hour, start_minute = start_time.split(':')
        start_hour = int(start_hour)
        start_minute = int(start_minute)
    else:
        start_hour = int(start_time)
    if ':' in finish_time:
        finish_hour, finish_minute = finish_time.split(':')
        finish_hour = int(finish_hour)
        finish_minute = int(finish_minute)
    else:
        finish_hour = int(finish_time)
    a = start_hour * 60 + start_minute
    b = finish_hour * 60 + finish_minute
    n_minutes = b - a
    # print(f'{line} -> {n_minutes}')
    assert n_minutes > 0
    return n_minutes


if __name__ == '__main__':
    assert len(sys.argv) == 2
    file_name = sys.argv[1]
    work_time_in_minutes = 0
    with open(file_name) as txt_file:
        for line in txt_file:
            n_minutes = count_minutes(line)
            work_time_in_minutes += n_minutes
    print(f'Work time: {work_time_in_minutes} minutes')
    print(f'Work time: {work_time_in_minutes // 60}:{work_time_in_minutes % 60:02d}')
    print(f'Estimated cost: {(work_time_in_minutes // 60) * 15000} Ft')

